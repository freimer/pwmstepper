import atexit
import logging

import pigpio

log = logging.getLogger()
log.setLevel(logging.DEBUG)
log.addHandler(logging.StreamHandler())

class Stepper:
    def __init__(self, gpio_pins=(4, 17, 23, 24), host='localhost', port=8888):
        self.pi = pigpio.pi(host, port)
        if not self.pi.connected:
            log.fatal('Not connected to pigpiod')
            exit()
        atexit.register(self._atexit)
        (self.IN1, self.IN2, self.IN3, self.IN4) = gpio_pins
        for pin in gpio_pins:
            self.pi.set_mode(pin, pigpio.OUTPUT)
            log.debug(f'pin {pin} set to output')

    def _atexit(self):
        log.debug('Performing cleanup')
        self.pi.stop()

    def generate_ramp(self, ramp):
        """Generate ramp wave forms.
        ramp:  List of [Frequency, Steps]
        """
        self.pi.wave_clear()     # clear existing waves
        chain = []

        # Generate a wave per ramp level, add to chain
        for i in range(len(ramp)):
            (frequency, steps) = ramp[i]
            micros = int(500000 / frequency / 8)
            wf1 = []
            wf2 = []
            wf3 = []
            wf4 = []
            wf1.append(pigpio.pulse(1 << self.IN1, 0, micros * 2))  # pulse on
            wf1.append(pigpio.pulse(0, 1 << self.IN1, micros * 5))  # pulse off
            wf1.append(pigpio.pulse(1 << self.IN1, 0, micros))  # pulse on
            wf2.append(pigpio.pulse(0, 1 << self.IN2, micros))  # pulse off
            wf2.append(pigpio.pulse(1 << self.IN2, 0, micros * 3))  # pulse on
            wf2.append(pigpio.pulse(0, 1 << self.IN2, micros * 4))  # pulse off
            wf3.append(pigpio.pulse(0, 1 << self.IN3, micros * 3))  # pulse off
            wf3.append(pigpio.pulse(1 << self.IN3, 0, micros * 3))  # pulse on
            wf3.append(pigpio.pulse(0, 1 << self.IN3, micros * 2))  # pulse off
            wf4.append(pigpio.pulse(0, 1 << self.IN4, micros * 5))  # pulse off
            wf4.append(pigpio.pulse(1 << self.IN4, 0, micros * 3))  # pulse on
            self.pi.wave_add_generic(wf1)
            self.pi.wave_add_generic(wf2)
            self.pi.wave_add_generic(wf3)
            self.pi.wave_add_generic(wf4)
            wid = self.pi.wave_create()
            chain += [255, 0, wid, 255, 1, steps & 255, steps >> 8]
        return chain
    
    def send_chain(self, chain):
        self.pi.wave_chain(chain)


#stepper = Stepper(gpio_pins=(6, 13, 16, 26))
stepper = Stepper()
chain = stepper.generate_ramp([[10,512], [20, 512]])
log.debug(chain)
stepper.send_chain(chain)
